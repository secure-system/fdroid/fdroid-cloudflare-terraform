resource "cloudflare_ruleset" "terraform_managed_resource_4ce703b334ca47cdb58666234eb3ca4a" {
  kind    = "zone"
  name    = "default"
  phase   = "http_request_transform"
  zone_id = var.zone_id
  rules {
    action = "rewrite"
    action_parameters {
      uri {
        path {
          value = "/search/static/search.svg"
        }
      }
    }
    description = "rewrite search icon"
    enabled     = true
    expression  = "(http.request.uri.path eq \"/static/search.svg\" and http.host eq \"cloudflare.${var.domain}\")"
    ref         = "c1b9ddca0f2d4d919fc1239ac6cc976b"
  }
  rules {
    action = "rewrite"
    action_parameters {
      uri {
        path {
          value = "/search/static/roboto.ttf"
        }
      }
    }
    description = "rewrite search font"
    enabled     = true
    expression  = "(http.host eq \"cloudflare.${var.domain}\" and http.request.uri.path eq \"/static/roboto.ttf\")"
    ref         = "5e1f018b69554af3b03be4d698a2a9a7"
  }
}

resource "cloudflare_ruleset" "terraform_managed_resource_6b0de9de83e04f79a576d9163e1bc80a" {
  kind    = "zone"
  name    = "default"
  phase   = "http_response_headers_transform"
  zone_id = var.zone_id
  rules {
    action = "rewrite"
    action_parameters {
      headers {
        name      = "Content-Type"
        operation = "set"
        value     = "application/vnd.android.package-archive"
      }
    }
    description = "apk content type"
    enabled     = true
    expression  = "(http.request.uri.path matches \".apk$\")"
    ref         = "d6bd932d463a4a88aad0562f06786e40"
  }
  rules {
    action = "rewrite"
    action_parameters {
      headers {
        name      = "content-security-policy"
        operation = "set"
        value     = "default-src 'none';base-uri 'self';block-all-mixed-content;connect-src 'self';font-src 'self';form-action 'self' https://search.${var.domain};frame-ancestors 'self';img-src 'self' https://${var.domain} https://fdroid.gitlab.io;media-src 'self';script-src 'self' https://ajax.cloudflare.com;style-src 'self' 'unsafe-inline';"
      }
    }
    description = "allow cloudflare rocket loader"
    enabled     = false
    expression  = "(not http.request.uri.path matches \"^/repo/\")"
    ref         = "6b77f1174ead412dbb1e06e58b4ebecb"
  }
}

resource "cloudflare_ruleset" "terraform_managed_resource_cc7775054cf4468a9932edc7224a3f68" {
  kind    = "zone"
  name    = "zone"
  phase   = "http_request_firewall_managed"
  zone_id = var.zone_id
  rules {
    action = "skip"
    action_parameters {
      ruleset = "current"
    }
    description = "Rule migrated from Firewall Rules: bypass: [79b7f0e3f1cc4172814a35742415f198]"
    enabled     = true
    expression  = "((http.user_agent eq \"Checkly/1.0 (https://www.checklyhq.com)\" and cf.client.bot) or (http.user_agent contains \"Checkly, https://www.checklyhq.com\" and cf.client.bot))"
    logging {
      enabled = true
    }
    ref = "7973e757c350402297075b378bafd14b"
  }
  rules {
    action = "execute"
    action_parameters {
      id = "efb7b8c949ac4650a09736fc376e9aee"
      overrides {
        rules {
          action  = "block"
          enabled = false
          id      = "47631a04883d4d7cab6bd7b83478adcb"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "977ad8daef224ecdbe475c7ab3ab3365"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "bdd776b4f296477f960acc346dfa618e"
        }
        rules {
          action  = "log"
          enabled = false
          id      = "49449f901cab4a01b2591ab836babcca"
        }
        rules {
          action  = "log"
          enabled = false
          id      = "d6f6d394cb01400284cfb7971e7aed1e"
        }
        rules {
          action  = "log"
          enabled = false
          id      = "d9aeff22f1024655937e5b033a61fbc5"
        }
        rules {
          action  = "log"
          enabled = false
          id      = "525329e705aa4fa596e126366d02615e"
        }
        rules {
          action  = "log"
          enabled = false
          id      = "8bb4bf582f704b61980fceff442561a8"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "15b0616fe67a439a8a3852410cadd290"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "00bee3de44184f7f8a6ad10910f04e13"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "7b1cfed7fd4047c6949c4d054751ef80"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "d8c7dbf00ec546e48e3c4340486c3ee2"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "ff8b8608c2c14bf5b3de621b6fc2309c"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "71b7793c77e24287861b82f0ec97cf32"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "40cba5ee3a014208958da0855ddfd8e3"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "5dd38056f4cd43fca7f198e6384f1856"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "d384de3d016d414dbf4d14caaa83212b"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "4a6e23c2acfe4b979b8c4d856d4d8e84"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "55b100786189495c93744db0e1efdffb"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "db1f213645904ab9b16b227b4a6a7b3a"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "6a05218f9dba40ed8eb4e89b12e7bc07"
        }
        rules {
          action  = "log"
          enabled = false
          id      = "aa3411d5505b4895b547d68950a28587"
        }
        rules {
          action  = "block"
          enabled = false
          id      = "ac89e3a915594a139fc370dece6a8e28"
        }
      }
      version = "latest"
    }
    enabled    = true
    expression = "true"
    ref        = "39a913e351364cf7a2080494c5d9d4f5"
  }
  rules {
    action = "execute"
    action_parameters {
      id = "4814384a9e5d4991b9815dcfc25d2f1f"
      overrides {
        categories {
          category = "paranoia-level-3"
          enabled  = false
        }
        categories {
          category = "paranoia-level-4"
          enabled  = false
        }
        rules {
          action          = "log"
          id              = "6179ae15870a4bb7b2d480d4843b323c"
          score_threshold = 25
        }
      }
      version = "latest"
    }
    enabled    = true
    expression = "true"
    ref        = "da2de981061c43f0be70d7095e8b1807"
  }
  rules {
    action = "execute"
    action_parameters {
      id      = "c2e184081120413c86c3ab7e14069605"
      version = "latest"
    }
    enabled    = true
    expression = "true"
    ref        = "959c30a8b0f2419885d9cc89dba8dbb6"
  }
}

resource "cloudflare_ruleset" "terraform_managed_resource_0b644370c08e4dd482e368bfef8769b9" {
  kind    = "zone"
  name    = "default"
  phase   = "http_request_firewall_custom"
  zone_id = var.zone_id
  rules {
    action = "skip"
    action_parameters {
      products = ["waf", "bic"]
    }
    description = "Allow Checkly"
    enabled     = true
    expression  = "(http.user_agent eq \"Checkly/1.0 (https://www.checklyhq.com)\" and cf.client.bot) or (http.user_agent contains \"Checkly, https://www.checklyhq.com\" and cf.client.bot)"
    logging {
      enabled = true
    }
    ref = "bed089900f7d43f9ab81ca7067a4f881"
  }
  rules {
    action = "skip"
    action_parameters {
      ruleset = "current"
    }
    description = "Allow tor"
    enabled     = false
    expression  = "(ip.geoip.country eq \"T1\")"
    logging {
      enabled = true
    }
    ref = "5c8bcf242e2745b9b8702e232861f8e8"
  }
  rules {
    action      = "managed_challenge"
    description = "challenge bots"
    enabled     = false
    expression  = "(cf.client.bot)"
    ref         = "227888553c814ab9876f31a34f85ca2b"
  }
  rules {
    action      = "block"
    description = "Block path attack"
    enabled     = true
    expression  = "(http.request.uri.path eq \"/forums/index.html\") or (http.request.uri.path eq \"/repo/null\")"
    ref         = "0f6c1ea2843948ea88d6bc441951b224"
  }
}

resource "cloudflare_ruleset" "terraform_managed_resource_04f3dcf4ec1640e49e44de8dde9e42a8" {
  kind    = "zone"
  name    = "default"
  phase   = "http_config_settings"
  zone_id = var.zone_id
  rules {
    action = "set_config"
    action_parameters {
      sxg = true
    }
    description = "search"
    enabled     = true
    expression  = "(starts_with(http.request.uri.path, \"/search\"))"
    ref         = "bc8128a8f7c74229b6b85f777ec8cf67"
  }
  rules {
    action = "set_config"
    action_parameters {
      sxg = true
    }
    description = "pages"
    enabled     = true
    expression  = "(http.request.uri.path contains \"/contribute/\") or (http.request.uri.path contains \"/docs/\") or (http.request.uri.path contains \"/about/\") or (http.request.uri.path contains \"/packages/\") or (http.request.uri.path contains \"/news/\") or (ends_with(http.request.uri.path, \".html\"))"
    ref         = "ebb423f4e90840a18e0ffe9fde952521"
  }
}

resource "cloudflare_ruleset" "terraform_managed_resource_65217b9aa07f486a930c4f68d88bf89e" {
  kind    = "zone"
  name    = "default"
  phase   = "http_ratelimit"
  zone_id = var.zone_id
  rules {
    action      = "managed_challenge"
    description = "rate limit verified bots"
    enabled     = true
    expression  = "(cf.bot_management.verified_bot)"
    ratelimit {
      characteristics     = ["cf.colo.id", "ip.src"]
      period              = 10
      requests_per_period = 100
      requests_to_origin  = true
      mitigation_timeout  = 0
    }
    ref = "a844d91c79724c14b417dfbdb7f5ba4a"
  }
}

resource "cloudflare_ruleset" "terraform_managed_resource_f900bfab24b74246a18cc9cf0f377d00" {
  kind    = "zone"
  name    = "default"
  phase   = "http_request_cache_settings"
  zone_id = var.zone_id
  rules {
    action = "set_cache_settings"
    action_parameters {
      browser_ttl {
        default = 2678400
        mode    = "override_origin"
      }
      cache = true
      edge_ttl {
        default = 31536000
        mode    = "override_origin"
        status_code_ttl {
          status_code_range {
            from = 400
          }
          value = 1
        }
        status_code_ttl {
          status_code_range {
            to = 301
          }
          value = 31536000
        }
      }
    }
    description = "static assets"
    enabled     = true
    expression  = "(starts_with(http.request.uri.path, \"/assets/\")) or (starts_with(http.request.uri.path, \"/css/\")) or (http.request.uri.path matches \"^/repo/.*.(png|jpg)$\") or (http.request.uri.path contains \"/static/\")"
    ref         = "78301ef4d000429581402f8e77edc2bf"
  }
  rules {
    action = "set_cache_settings"
    action_parameters {
      browser_ttl {
        default = 3600
        mode    = "override_origin"
      }
      cache = true
      edge_ttl {
        default = 86400
        mode    = "override_origin"
      }
      respect_strong_etags = true
    }
    description = "index"
    enabled     = true
    expression  = "(starts_with(http.request.uri.path, \"/repo/index\")) or (starts_with(http.request.uri.path, \"/repo/entry\"))"
    ref         = "3f987de954e94211ac9601b7844a728f"
  }
  rules {
    action = "set_cache_settings"
    action_parameters {
      browser_ttl {
        default = 3600
        mode    = "override_origin"
      }
      cache = true
      edge_ttl {
        default = 31536000
        mode    = "override_origin"
      }
      respect_strong_etags = true
    }
    description = "packages"
    enabled     = true
    expression  = "(http.request.uri.path matches \"^/repo/.*.apk$\") or (http.request.uri.path matches \"^/repo/.*.gz$\")"
    ref         = "cd1f754dae334955a39e0838f3993a42"
  }
  rules {
    action = "set_cache_settings"
    action_parameters {
      browser_ttl {
        default = 3600
        mode    = "override_origin"
      }
      cache = true
      edge_ttl {
        default = 86400
        mode    = "override_origin"
      }
    }
    description = "search"
    enabled     = true
    expression  = "(starts_with(http.request.uri.path, \"/search\"))"
    ref         = "a5ef3b273efb490499f5036d8781ec40"
  }
  rules {
    action = "set_cache_settings"
    action_parameters {
      browser_ttl {
        default = 3600
        mode    = "override_origin"
      }
      cache = true
      edge_ttl {
        default = 259200
        mode    = "override_origin"
      }
    }
    description = "pages"
    enabled     = true
    expression  = "(http.request.uri.path contains \"/contribute/\") or (http.request.uri.path contains \"/docs/\") or (http.request.uri.path contains \"/about/\") or (http.request.uri.path contains \"/packages/\") or (http.request.uri.path contains \"/news/\") or (ends_with(http.request.uri.path, \".html\"))"
    ref         = "7b35dfb893bc47428163725f92e9dc6c"
  }
}

resource "cloudflare_ruleset" "terraform_managed_resource_ec36846b1bfa49afa159beb58158d2ef" {
  description = "ruleset for controlling url normalization"
  kind        = "zone"
  name        = "Entrypoint for url normalization ruleset"
  phase       = "http_request_sanitize"
  zone_id     = var.zone_id
  rules {
    action = "execute"
    action_parameters {
      id = "70339d97bdb34195bbf054b1ebe81f76"
      overrides {
        rules {
          enabled = false
          id      = "78723a9e0c7c4c6dbec5684cb766231d"
        }
        rules {
          enabled = true
          id      = "b232b534beea4e00a21dcbb7a8a545e9"
        }
        rules {
          enabled = false
          id      = "20e18610e4a048d6b87430b3cb2d89a3"
        }
        rules {
          enabled = false
          id      = "60444c0705d4438799584a15cca2cb7d"
        }
      }
      version = "latest"
    }
    enabled    = true
    expression = "true"
    ref        = "03c8273ebf46485681eeb20d9c69106f"
  }
}
