terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4.0"
    }
  }

  # to use the GitLab-managed Terraform state
  # https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html#initialize-a-terraform-state-as-a-backend-by-using-gitlab-cicd 
  backend "http" {
  }
}

provider "cloudflare" {
  # token pulled from $CLOUDFLARE_API_TOKEN
}

variable "zone_id" {
  default     = "ea64d8a35c7ec6f35de3f79b3262ef71"
  type        = string
  description = "The Zone ID of the domain."
}

variable "domain" {
  default     = "f-droid.org"
  type        = string
  description = "The domain name of the site."
}

resource "cloudflare_zone_settings_override" "dns_settings" {
  zone_id = var.zone_id

  settings {
    cname_flattening = "flatten_at_root"
  }
}

resource "cloudflare_zone_settings_override" "tls_settings" {
  zone_id = var.zone_id

  settings {
    tls_1_3                  = "zrt"
    automatic_https_rewrites = "on"
    ssl                      = "strict"
    always_online            = "on"
    always_use_https         = "on"
    opportunistic_encryption = "on"
    min_tls_version          = "1.2"

    # HSTS
    security_header {
      enabled = true
      preload = true

      # 6 months
      max_age = 15780000

      include_subdomains = true
      nosniff            = true
    }
  }
}



resource "cloudflare_authenticated_origin_pulls" "authenticated_origin_pulls" {
  zone_id = var.zone_id
  enabled = true
}


resource "cloudflare_zone_settings_override" "security_settings" {
  zone_id = var.zone_id

  settings {
    security_level = "essentially_off"
    # 1 year
    challenge_ttl = 31536000
    privacy_pass  = "off"
  }
}

resource "cloudflare_zone_settings_override" "speed_settings" {
  zone_id = var.zone_id

  settings {
    image_resizing = "off"
    polish         = "lossy"

    # it prevents images to be cached
    webp = "off"

    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }
    brotli            = "on"
    early_hints       = "on"
    h2_prioritization = "on"

    # it breaks all images on mobile devices
    mirage = "off"

    # it breaks the screenshot slider on mobile devices
    rocket_loader = "off"
  }
}


resource "cloudflare_zone_settings_override" "cache_settings" {
  zone_id = var.zone_id

  settings {
    # 1 month
    browser_cache_ttl = 2678400
  }
}

resource "cloudflare_tiered_cache" "tired_cache" {
  zone_id    = var.zone_id
  cache_type = "smart"
}

resource "cloudflare_url_normalization_settings" "url_normalization_settings" {
  zone_id = var.zone_id
  type    = "cloudflare"
  scope   = "both"
}


resource "cloudflare_zone_settings_override" "network_settings" {
  zone_id = var.zone_id
  settings {
    http3      = "on"
    max_upload = 200
    websockets = "on"
    zero_rtt   = "on"
  }
}


resource "cloudflare_zone_settings_override" "scrape_shield_settings" {
  zone_id = var.zone_id
  settings {
    email_obfuscation   = "off"
    server_side_exclude = "off"
    hotlink_protection  = "off"
  }
}

resource "cloudflare_worker_domain" "worker_domain_settings" {
  hostname   = "cloudflare.f-droid.org"
  service    = "fdroid-production"
  zone_id    = var.zone_id
  account_id = local.account_id
}
