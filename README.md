# F-droid Cloudflare Terraform Deployment

## Prerequisites

- cf-terraforming
    ```
    GO111MODULE=on go install github.com/cloudflare/cf-terraforming/cmd/cf-terraforming@latest
    ```

## Sync existing settings from Cloudflare

1. Connect to the GitLab managed Terraform state by running the script generated in https://gitlab.com/secure-system/fdroid/fdroid-cloudflare-terraform/-/terraform (or your fork's corresponding page).

2. Use [cf-terraforming](https://github.com/cloudflare/cf-terraforming) to download the existing settings.

   ```
   # if using API Token
   export CLOUDFLARE_API_TOKEN='<your-api-token>'

   export CLOUDFLARE_ZONE_ID='<your-zone-id>'

   # now call cf-terraforming, e.g.
   cf-terraforming generate \
     --resource-type "cloudflare_page_rule" \
     --zone "${CLOUDFLARE_ZONE_ID}" > page_rules.tf
   ```

3. Generate the state import commands and execute the generated commands.

   ```
      cf-terraforming import \
        --resource-type "cloudflare_page_rule" \
        --zone "${CLOUDFLARE_ZONE_ID}"
   ```
