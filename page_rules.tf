resource "cloudflare_page_rule" "terraform_managed_resource_9c5be8a727dae12d6ce126a068c9b7ee" {
  priority = 16
  status   = "disabled"
  target   = "*${var.domain}/wiki/index.php*"
  zone_id  = var.zone_id
  actions {
    forwarding_url {
      status_code = 301
      url         = "https://cloudflare.${var.domain}/wiki/page/Main_Page"
    }
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_e5ca27902ae3159935250ba5b39c4d3d" {
  priority = 15
  status   = "disabled"
  target   = "cloudflare.${var.domain}/static/*"
  zone_id  = var.zone_id
  actions {
    forwarding_url {
      status_code = 301
      url         = "https://cloudflare.${var.domain}/search/static/$1"
    }
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_ce0d14c2ac733d6f295a1d26ab39838f" {
  priority = 14
  status   = "disabled"
  target   = "*.${var.domain}/repo"
  zone_id  = var.zone_id
  actions {
    forwarding_url {
      status_code = 301
      url         = "https://$1.${var.domain}/repo/"
    }
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_899eb5bb78f36e7dcb01833363928a65" {
  priority = 13
  status   = "disabled"
  target   = "*${var.domain}/assets/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 31536000
    cache_level            = "cache_everything"
    edge_cache_ttl         = 31536000
    explicit_cache_control = "off"
    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }
    polish = "lossless"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_036b7700175dd0b238547acd24578b7f" {
  priority = 12
  status   = "disabled"
  target   = "*${var.domain}/css/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 31536000
    edge_cache_ttl         = 31536000
    explicit_cache_control = "off"
    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_e5c79d87ba9645a17eadaec8a26ce7e4" {
  priority = 11
  status   = "disabled"
  target   = "*${var.domain}/repo/*.png"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 31536000
    cache_level            = "cache_everything"
    edge_cache_ttl         = 31536000
    explicit_cache_control = "off"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_2f755778c6abbed7c352399e07b6ab4c" {
  priority = 10
  status   = "disabled"
  target   = "*${var.domain}/repo/index*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 86400
    cache_level            = "cache_everything"
    edge_cache_ttl         = 86400
    explicit_cache_control = "off"
    mirage                 = "off"
    polish                 = "lossy"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_3fc15c52829ed4beaa99539986e29e90" {
  priority = 9
  status   = "disabled"
  target   = "*${var.domain}/repo/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 31536000
    cache_level            = "cache_everything"
    edge_cache_ttl         = 31536000
    explicit_cache_control = "off"
    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }
    polish = "lossy"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_8a873a30bb2f4b25b68a636850ea052b" {
  priority = 8
  status   = "disabled"
  target   = "*${var.domain}/*/packages/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 28800
    cache_level            = "cache_everything"
    edge_cache_ttl         = 86400
    explicit_cache_control = "off"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_231aeb007f390dc1222986d96ffc6612" {
  priority = 7
  status   = "disabled"
  target   = "*${var.domain}/*/about/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 86400
    cache_level            = "cache_everything"
    edge_cache_ttl         = 2678400
    explicit_cache_control = "off"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_c236403bde864e53fef92a5137aae15e" {
  priority = 6
  status   = "disabled"
  target   = "*${var.domain}/*/docs/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 28800
    cache_level            = "cache_everything"
    edge_cache_ttl         = 31536000
    explicit_cache_control = "off"
    minify {
      css  = "on"
      html = "on"
      js   = "on"
    }
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_a6f58d51e0cd7544c84656255d18d73c" {
  priority = 5
  status   = "disabled"
  target   = "*${var.domain}/*/news/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 86400
    edge_cache_ttl         = 31536000
    explicit_cache_control = "off"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_003143ef9d46b16e8ea567b6dd496de9" {
  priority = 4
  status   = "disabled"
  target   = "*${var.domain}/*/contribute/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 86400
    edge_cache_ttl         = 31536000
    explicit_cache_control = "off"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_1e37de45cfe062d20c0b4dbb427ac359" {
  priority = 3
  status   = "disabled"
  target   = "*${var.domain}/search/*"
  zone_id  = var.zone_id
  actions {
    browser_cache_ttl      = 3600
    cache_level            = "cache_everything"
    edge_cache_ttl         = 86400
    explicit_cache_control = "off"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_9e911859bf4a555872b9eabc678615ba" {
  priority = 2
  status   = "active"
  target   = "*${var.domain}/*?*static=false*"
  zone_id  = var.zone_id
  actions {
    cache_level            = "bypass"
    explicit_cache_control = "off"
  }
}

resource "cloudflare_page_rule" "terraform_managed_resource_8c4fd90de41a410d7a242658b58a9967" {
  priority = 1
  status   = "active"
  target   = "*${var.domain}/*"
  zone_id  = var.zone_id
  actions {
    cache_level = "cache_everything"
  }
}

